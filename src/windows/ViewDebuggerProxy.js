/*
@date  : 11.05.2015
@author: Pavel Durov
@note  :  In order to use ViewDebuggerProxy on Universal Windows Platform -
		  Make sure that you are referencing base.js and ui.js 
 */
function ViewDebugger() { /* CTOR...*/ }


ViewDebugger.prototype.show = function (options, successCallback, errorCallback) {
	
    ViewDebugger.prototype.successCallBack = successCallback;
    ViewDebugger.prototype.errorCallback = errorCallback;

    if (cordova.platformId == "windows") {
        ViewDebugger.prototype._injectWinJsFlyoutHTML();
    }

    if (options) {
        var actionSheetProxyFlyoutDiv = document.getElementById("actionSheetProxyFlyoutDiv");
        //Settings popup
        this._clearChildren(actionSheetProxyFlyoutDiv);

        if (options.title) {    //Append Title
            this._addTitle(options.title, actionSheetProxyFlyoutDiv);
        }

        if (!options.destructiveButtonLast && options.addDestructiveButtonWithLabel) {    //Generate Desctructive Button
            this._addDestructiveButton(options.addDestructiveButtonWithLabel, actionSheetProxyFlyoutDiv);
        }

        if (options.buttonLabels && options.buttonLabels instanceof Array) {    //Appending Buttons to main Flyout Div
            this._addbuttons(options.buttonLabels, actionSheetProxyFlyoutDiv);
        }

        if (options.destructiveButtonLast && options.addDestructiveButtonWithLabel) {    //Generate Desctructive Button
            this._addDestructiveButton(options.addDestructiveButtonWithLabel, actionSheetProxyFlyoutDiv);
        }

        if (options.winphoneEnableCancelButton && options.addCancelButtonWithLabel) {

            this._addCancelButton(options.addCancelButtonWithLabel, actionSheetProxyFlyoutDiv);
        }
        
        var fly = document.getElementById("fly-test");
        WinJS.UI.process(fly).done(function () {

            if (fly && fly.winControl) {
                //var bodys = document.getElementsByClassName('bodyClass')[0];
                var anchor = document.getElementById("actionSheetSetPoint");
                if (anchor) {
                    fly.winControl.show(anchor, "bottom");
                }


            } else {
                console.log("winControl is undefined");
            }
        });
    }

    document.addEventListener('backbutton', window.plugins.actionsheet.hide, false)
};


ViewDebugger.prototype.hide = function (options, successCallback, errorCallback) {
    console.log("//\\//\\//\\//\\//\\ ViewDebugger.prototype.hide ");

    var fly = document.getElementById("fly-test");

    WinJS.UI.process(fly).done(function () {
        if (fly.winControl && fly.winControl.hide) {
            //WinJS.UI.Animation.hidePopup(fly).done( /* Your success and error handlers */);
            fly.winControl.hide();
        }
    });
};



ViewDebugger.prototype.ANDROID_THEMES = {
    THEME_TRADITIONAL: 1, // default
    THEME_HOLO_DARK: 2,
    THEME_HOLO_LIGHT: 3,
    THEME_DEVICE_DEFAULT_DARK: 4,
    THEME_DEVICE_DEFAULT_LIGHT: 5
};

ViewDebugger.prototype.install = function () {
    if (!window.plugins) {
        window.plugins = {};
    }
    window.plugins.actionsheet = new ViewDebugger();

    return window.plugins.actionsheet;
};

cordova.addConstructor(ViewDebugger.prototype.install);
cordova.commandProxy.add("ViewDebugger", ViewDebugger);


//Helpers


ViewDebugger.prototype._addTitle = function (label, destinationCtrl) {
    var textCtrl = this._generateTitle(label)
    destinationCtrl.appendChild(textCtrl);
}


ViewDebugger.prototype._addDestructiveButton = function (label, destinationCtrl) {
    if (label && destinationCtrl) {
        var destructive_btn = this._generateButton(label, this._getDestructiveButtonStyle());
        destinationCtrl.appendChild(destructive_btn);
    }
}

ViewDebugger.prototype._addCancelButton = function (label, destinationCtrl) {
    if (label && destinationCtrl) {
        var cancel_btn = this._generateButton(label, this._getCancelButtonStyle());
        cancel_btn.onclick = function () {
            ViewDebugger.prototype.hide();
        }

        destinationCtrl.appendChild(cancel_btn);
    }
}

ViewDebugger.prototype._addbuttons = function (lables, destinationCtrl) {
    if (lables && destinationCtrl) {
        for (var i = 0; i < lables.length; i++) {
            var btn = this._generateButton(lables[i], this._getButtonStyle());
	    (function (i) {
            	btn.onclick = function () {
                    if (ViewDebugger.prototype.successCallBack) {
                    	ViewDebugger.prototype.successCallBack(i + 1);
                    	ViewDebugger.prototype.hide();
                    }                
            	}
            })(i);
            destinationCtrl.appendChild(btn);
        }
    }
}

ViewDebugger.prototype._clearChildren = function (element) {
    if (element && element.hasChildNodes) {
        while (element.hasChildNodes()) {
            element.removeChild(element.lastChild);
        }
    }
}

ViewDebugger.prototype._generateButton = function (content, style) {

    var btn = document.createElement("input");
    btn.setAttribute("type", "button");
    btn.setAttribute("value", content);

    btn.setAttribute("style", style);//;

    return btn;
}


//Style
ViewDebugger.prototype._getButtonStyle = function () {
    return "display: table-row; font-size: 20px; background-color :black;" +
            " color: white; width: 98%; position: relative; margin: 1%; background: black; margin-top: 20px; height: 60px;"
}
//The only diffrence beweet regulat button and cancel vuttokn styles is WIDTH!!
ViewDebugger.prototype._getCancelButtonStyle = function () {
    return "display: table-row; font-size: 20px; background-color :black;" +
            " color: white; width: 49%; position: relative; margin: 1%; background: black; margin-top: 20px; height: 60px;"
}
//The only diffrence beweet destructive button and regular is its color (orange)!
ViewDebugger.prototype._getDestructiveButtonStyle = function () {
    return "display: table-row; font-size: 20px; background-color : black;" +
            " color: orange; width: 98%; position: relative; margin: 1%; background: black; margin-top: 20px; height: 60px;"
}



ViewDebugger.prototype._getMainDivStyle = function () {
    return "position:absolute; width:99%; display: table; background-color: #4F4F4F; padding: 15px; visibility:collapse ; text-align : center;";
}

ViewDebugger.prototype._getTitleStyle = function () {
    return "color:white; font-size: 30px; ";
}

ViewDebugger.prototype._generateTitle = function (textLebel) {
    var spanCtrl = document.createElement("span");
    spanCtrl.setAttribute("style", this._getTitleStyle());
    spanCtrl.innerHTML = textLebel;
    return spanCtrl;
}

ViewDebugger.prototype._injectWinJsFlyoutHTML = function () {
    var divSetPoint = document.createElement("div");
    divSetPoint.setAttribute("id", "actionSheetSetPoint");
    divSetPoint.setAttribute("aria-haspopup", "true");
    divSetPoint.setAttribute("style", "visibility:collapse");

    var flyoutDiv = document.createElement("div");
    flyoutDiv.setAttribute("data-win-control", "WinJS.UI.Flyout");
    flyoutDiv.setAttribute("id", "fly-test");
    flyoutDiv.setAttribute("style", this._getMainDivStyle());

    var internalDiv = document.createElement("div");
    internalDiv.setAttribute("id", "actionSheetProxyFlyoutDiv");

    flyoutDiv.appendChild(internalDiv);

    document.body.appendChild(flyoutDiv);
    document.body.appendChild(divSetPoint);
}
