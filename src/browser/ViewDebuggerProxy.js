function ViewDebugger() {
}

ViewDebugger.prototype.show = function (options, successCallback, errorCallback) {

    if (successCallback) {
        ViewDebugger.prototype.successCallBack = successCallback;
    }

    var actionSheetContainer = document.getElementById('actionSheetProxyContainer');
    if (!actionSheetContainer) {
        var body = document.getElementsByTagName('body')[0];
        actionSheetContainer = document.createElement('div');
        actionSheetContainer.setAttribute('class', 'action-sheet-container');
        actionSheetContainer.setAttribute('id', 'actionSheetProxyContainer');
        body.appendChild(actionSheetContainer);
    }

    if (actionSheetContainer.hidden) {
        actionSheetContainer.hidden = false;
    }

    if (options) {
        this._clearChildren(actionSheetContainer);

        if (options.title) {
            this._addTitle(options.title, actionSheetContainer);
        }

        if (!options.destructiveButtonLast && options.addDestructiveButtonWithLabel) {
            this._addDestructiveButton(options.addDestructiveButtonWithLabel, actionSheetContainer, 1);
            ViewDebugger.prototype._btnOffsetIndex = 2;
        } else {
            ViewDebugger.prototype._btnOffsetIndex = 1;
        }

        if (options.buttonLabels) {
            this._addbuttons(options.buttonLabels, actionSheetContainer);
        }

        if (options.destructiveButtonLast && options.addDestructiveButtonWithLabel) {    //Generate Desctructive Button
            this._addDestructiveButton(options.addDestructiveButtonWithLabel, actionSheetContainer, options.buttonLabels.length + 1);
        }

        if (options.addCancelButtonWithLabel) {
            this._addCancelButton(options.addCancelButtonWithLabel, actionSheetContainer);
        }

    }
};

ViewDebugger.prototype.hide = function (options, successCallback, errorCallback) {
    var actionSheetContainer = document.getElementById('actionSheetProxyContainer');
    actionSheetContainer.hidden = true;
};

ViewDebugger.prototype.install = function () {
    if (!window.plugins) {
        window.plugins = {};
    }
    window.plugins.actionsheet = new ViewDebugger();

    return window.plugins.actionsheet;
};

cordova.addConstructor(ViewDebugger.prototype.install);
cordova.commandProxy.add('ViewDebugger', ViewDebugger);

//Helpers
ViewDebugger.prototype._addTitle = function (label, destination) {
    var title = document.createElement('h3');
    title.setAttribute('class', 'action-sheet-title');
    title.innerHTML = label;
    destination.appendChild(title);
};


ViewDebugger.prototype._addDestructiveButton = function (label, destination, position) {
    var btn = document.createElement('button');
    btn.setAttribute('value', position);
    btn.setAttribute('class', 'action-sheet-button action-sheet-destructive-button');
    btn.innerHTML = label;
    btn.onclick = ViewDebugger.prototype._onclick;
    destination.appendChild(btn);
};

ViewDebugger.prototype._addCancelButton = function (label, destination) {
    var btn = document.createElement('button');
    btn.setAttribute('class', 'action-sheet-button action-sheet-cancel-button');
    btn.innerHTML = label;
    btn.onclick = function () {
        ViewDebugger.prototype.hide();
    };
    destination.appendChild(btn);
};

ViewDebugger.prototype._addbuttons = function (labels, destination) {
    for (var i = 0; i < labels.length; i++) {
        var btn = document.createElement('button');
        btn.setAttribute('value', i + ViewDebugger.prototype._btnOffsetIndex);
        btn.setAttribute('class', 'action-sheet-button action-sheet-normal-button');
        btn.innerHTML = labels[i];
        btn.onclick = ViewDebugger.prototype._onclick;
        destination.appendChild(btn);
    }
};

ViewDebugger.prototype._onclick = function (ev) {
    ev.preventDefault();
    ev.stopPropagation();
    ViewDebugger.prototype.hide();
    if (ViewDebugger.prototype.successCallBack) {
        ViewDebugger.prototype.successCallBack(parseInt(ev.target.value, 10));
    }
};

ViewDebugger.prototype._clearChildren = function (element) {
    if (element && element.hasChildNodes) {
        while (element.hasChildNodes()) {
            element.removeChild(element.lastChild);
        }
    }
};

ViewDebugger.prototype.ANDROID_THEMES = {
    THEME_TRADITIONAL: 1, // default
    THEME_HOLO_DARK: 2,
    THEME_HOLO_LIGHT: 3,
    THEME_DEVICE_DEFAULT_DARK: 4,
    THEME_DEVICE_DEFAULT_LIGHT: 5
};

