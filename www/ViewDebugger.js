function ViewDebugger() {
}

ViewDebugger.prototype.show = function (options, successCallback, errorCallback) {
  cordova.exec(successCallback, errorCallback, "ViewDebugger", "show", [options]);
};

ViewDebugger.prototype.hide = function (options, successCallback, errorCallback) {
  cordova.exec(successCallback, errorCallback, "ViewDebugger", "hide", [options]);
};

ViewDebugger.prototype.ANDROID_THEMES = {
  THEME_TRADITIONAL          : 1, // default
  THEME_HOLO_DARK            : 2,
  THEME_HOLO_LIGHT           : 3,
  THEME_DEVICE_DEFAULT_DARK  : 4,
  THEME_DEVICE_DEFAULT_LIGHT : 5
};

ViewDebugger.install = function () {
  if (!window.plugins) {
    window.plugins = {};
  }

  window.plugins.viewdebugger = new ViewDebugger();

  return window.plugins.actionsheet;
};

cordova.addConstructor(ViewDebugger.install);
